import React from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Toggle from 'material-ui/Toggle';
import styles from './index.scss';

const TimeSwitcher = ({ label, onToggle }) => {
  return (
    <div className={styles.wrapper}>
      <MuiThemeProvider>
        <Toggle
          label={label}
          onToggle={onToggle}
        />
      </MuiThemeProvider>
      <span className={styles.labelRight}>
        {label.right}
      </span>
      <span className={styles.labelLeft}>
        {label.left}
      </span>
    </div>
  )
};

TimeSwitcher.propTypes = {
  label: PropTypes.string,
  onToggle: PropTypes.func,
};

TimeSwitcher.defaultProps = {
  label: 'Label',
  onToggle: () => {},
};

export default TimeSwitcher;
