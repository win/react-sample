import React, { Component } from 'react';
import Button from 'material-ui/RaisedButton';
import DoneIcon from 'material-ui-icons/Done';
import PriceTable from '../PriceTable';
import TimeSwitcher from '../TimeSwitcher';
import styles from './index.scss';

class App extends Component {
  headerItems = [
    {
      id: 'plan1',
      content: 'Plan',
    },
    {
      id: 'plan2',
      content: 'Plan 2',
    },
    {
      id: 'plan3',
      content: 'Plan 3',
    },
  ];

  bodyItems = [
    [
      {
        headerId: 'plan1',
        content: 'Crowdbotics 1'
      },
      {
        headerId: 'plan2',
        content: 'Crowdbotics 2',
      },
      {
        headerId: 'plan3',
        content: 'Crowdbotics 3',
      },
    ],
    [
      {
        headerId: 'plan1',
        content: 'Market Trends'
      },
      {
        headerId: 'plan2',
        content: <DoneIcon className={styles.done}/>,
      },
      {
        headerId: 'plan3',
        content: <DoneIcon className={styles.done}/>,
      },
    ],
    [
      {
        headerId: 'plan1',
        content: 'Search Results'
      },
      {
        headerId: 'plan2',
        content: 200,
      },
      {
        headerId: 'plan3',
        content: 'Unlimited',
      },
    ],
    [
      {
        headerId: 'plan1',
        content: 'Search by deeds'
      },
      {
        headerId: 'plan2',
        content: <DoneIcon className={styles.done}/>,
      },
      {
        headerId: 'plan3',
        content: <DoneIcon className={styles.done}/>,
      },
    ],
  ];

  priceItems = [
    {
     headerId: 'plan1',
     content: 'Price',
    },
    {
      headerId: 'plan2',
      content: <div className={styles.priceWrapper}>
        <div className={styles.price}>$0</div>
        <Button
          className={styles.btn}
          onClick={this.onSignUp}
        >
          Sign Up
        </Button>
      </div>,
    },
    {
      headerId: 'plan3',
      content: <div className={styles.priceWrapper}>
        <div className={styles.price}>$10</div>
        <Button
          className={styles.btn}
          onClick={this.onStandardTrial}
        >
          Start Trial
        </Button>
      </div>,
    }
  ];

  footerItems = [
    {
      headerId: 'plan1',
      content: <Button
        className={styles.btn}
        onClick={this.onSignUp}
      >
        Sign Up
      </Button>,
    },
    {
      headerId: 'plan2',
      content: <Button
        className={styles.btn}
        onClick={this.onStandardTrial}
      >
        Start Trial
      </Button>,
    },
  ];

  onToggle = () => {
    alert('Toggle Action!');
  };

  onSignUp = () => {
    alert('Sign Up Action');
  };

  onStandardTrial = () => {
    alert('Standard Trial Action');
  };

  render() {
    return (
      <div className={styles.wrapper}>
        <TimeSwitcher
          label="Monthly/Annually"
          onToggle={this.onToggle}
        />
        <PriceTable
          headerItems={this.headerItems}
          bodyItems={this.bodyItems}
          priceItems={this.priceItems}
          footerItems={this.footerItems}
        />
      </div>
    );
  }
}

export default App;
