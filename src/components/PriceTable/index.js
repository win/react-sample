import React from 'react';
import Table, {
  TableBody, TableHeaderColumn, TableHeader, TableRow, TableRowColumn,
} from 'material-ui/Table';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import styles from './index.scss';

const getContentByHeaderId = (items, headerId) => {
  const found = items.find((item) => item.headerId === headerId);
  return found ? found.content : '';
};

const PriceTable = ({headerItems, bodyItems, priceItems, footerItems}) => {
  return (
    <div className={styles.wrapper}>
      <MuiThemeProvider>
        <Table>
          <TableHeader
            adjustForCheckbox={false}
            displaySelectAll={false}
            className={styles.headerRow}
          >
            <TableRow>
              {headerItems.map((item) => (
                <TableHeaderColumn
                  key={`header-${item.id}`}
                  className={styles.headerCell}
                >
                  {item.content}
                </TableHeaderColumn>
              ))}
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={false}
            className={styles.tableBody}
            showRowHover
          >
            <TableRow
              className={styles.priceRow}
            >
              {headerItems.map((headerItem) => (
                <TableRowColumn className={styles.priceCell}>
                  {getContentByHeaderId(priceItems, headerItem.id)}
                </TableRowColumn>
              ))}
            </TableRow>
            {bodyItems.map((items) => (
              <TableRow
                className={styles.bodyRow}
              >
                {headerItems.map((headerItem) => (
                  <TableRowColumn className={styles.bodyCell}>
                    {getContentByHeaderId(items, headerItem.id)}
                  </TableRowColumn>
                ))}
              </TableRow>
            ))}
            {headerItems.map((headerItem) => (
              <TableRowColumn className={styles.bodyCell}>
                {getContentByHeaderId(footerItems, headerItem.id)}
              </TableRowColumn>
            ))}
          </TableBody>
        </Table>
      </MuiThemeProvider>
    </div>
  );
};

export default PriceTable;
